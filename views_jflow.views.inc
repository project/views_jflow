<?php

/**
 * Implements hook_views_plugins().
 */
function views_jflow_views_plugins() {
  return array(
    'style' => array(
      'jflow' => array(
        'title' => t('jFlow'),
        'help' => t('Display the results using jFlow.'),
        'handler' => 'views_jflow_plugin_style_jflow',
        'theme' => 'views_jflow',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}


