<?php


class views_jflow_plugin_style_jflow extends views_plugin_style {

  function options(&$options) {  
    $options['width'] = '600px';
    $options['height'] = '200px';
    $options['duration'] = '400';
    $options['auto'] = TRUE;
    $options['delay'] = 3000;
    $options['prev'] = TRUE;
    $options['next'] = TRUE;
    $options['controller'] = TRUE;
    $options['control'] = "";
  }
  function options_form(&$form, &$form_state) {
    $options = $this->display->handler->get_field_labels();
    $form['width'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => t('Width'),
      '#description' => t('The width of the jFlow content area, specified in px or %.'),
      '#default_value' => $this->options['width'],
    );
    $form['height'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => t('Height'),
      '#description' => t('The height of the jFlow content area, specified in px or %.'),
      '#default_value' => $this->options['height'],
    );
    $form['duration'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => t('Duration'),
      '#description' => t('The number of milliseconds the transition should take.'),
      '#default_value' => $this->options['duration'],
    );
    $form['auto'] = array(
      '#type' => 'checkbox',
      '#title' => t('Automatic transition'),
      '#default_value' => $this->options['auto'],
    );
    $form['delay'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#title' => t('Delay'),
      '#description' => t('If auto transition is enabled, how many milliseconds between changes.'),
      '#default_value' => $this->options['delay'],
    );
    $form['prev'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show prev button'),
      '#default_value' => $this->options['prev'],
    );
    $form['next'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show next button'),
      '#default_value' => $this->options['next'],
    );
    $form['controller'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show controls'),
      '#default_value' => $this->options['controller'],
    );
    $form['control'] = array(
      '#type' => 'select',
      '#rows' => 3,
      '#title' => t('Control'),
      '#options' => $options,
      '#description' => t('Select which field is used to render as the control. Don\'t use a link or this will break the control.'),
      '#default_value' => $this->options['control'],
    );
  }
  function pre_render($result) {
    if (!empty($this->row_plugin)) {
      $this->row_plugin->pre_render($result);
    }
  }


}


