<div id="view_<?php print $view->name; ?>">
	<?php foreach ($rows as $row) { ?>
		<div class="slide-wrapper">
      <?php print $row; ?>
    </div>
  <?php } ?>
</div>
<div id="view_<?php print $view->name; ?>_controller"> 
  <span class="view_<?php print $view->name; ?>_control_prev"<?php if (!$options['prev']) print ' style="display:none;"'; ?>>Prev</span>
  <?php foreach ($controls as $control) { ?>
    <span class="view_<?php print $view->name; ?>_control"<?php if (!$options['controller']) print ' style="display:none;"'; ?>><?php print $control; ?></span>
  <?php } ?>
  <span class="view_<?php print $view->name; ?>_control_next"<?php if (!$options['next']) print ' style="display:none;"'; ?>>Next</span>
</div>

